## 0.2.1 (2018-09-22)

- Persist state between restarts
- Update webthing-node to 0.8.0
- Implement readOnly flag
