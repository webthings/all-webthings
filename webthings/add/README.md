# WebThing: Add
This is a WebThing to perform an addition of two numbers. Use it to add numbers in gateways which do not support adding two outputs.

![A preview image of the WebThing Add in the Mozilla Things Gateway](preview.png)

## Explanation
`A + B = Y`

Y is the sum of the modifiable properties A and B.

Property | Internal name | Type | Modifiable? |  Description
--- |
**A** | `a` | number | yes | First summand
**B** | `b` | number | yes | Second summand
**Y** | `y` | number | no | The sum of A and B
