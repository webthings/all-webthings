const {
  Property,
  Thing,
  Value,
} = require('webthing');

class WebThingAdd extends Thing {
  constructor(options = {}, state = {}) {
    super(
      options.id || Math.random().toString(),
      options.name || 'Math: Add',
      ['Thing'],
      options.description || 'Performs an addition of two numbers: A + B = Y'
    );

    this.loadState(state);
    this.result = new Value(this.state.a + this.state.b);

    this.addProperty(
      new Property(this,
        'a',
        new Value(this.state.a, this.setA.bind(this)),
        {
          title: 'A',
          type: 'number',
          description: 'First input',
        }
      )
    );

    this.addProperty(
      new Property(this,
        'b',
        new Value(this.state.b, this.setB.bind(this)),
        {
          title: 'B',
          type: 'number',
          description: 'Second input',
        }
      )
    );

    this.addProperty(
      new Property(this,
        'y',
        this.result,
        {
          title: 'Y',
          type: 'number',
          description: 'The result',
          readOnly: true,
        }
      )
    );
  }

  loadState(object) {
    this.state = {
      a: object.a || 0,
      b: object.b || 0,
    };
  }

  saveState() {
    return {
      a: this.state.a,
      b: this.state.b,
    };
  }

  setA(value) {
    this.state.a = value;
    this.updateResult();
  }

  setB(value) {
    this.state.b = value;
    this.updateResult();
  }

  updateResult() {
    this.result.notifyOfExternalUpdate(this.state.a + this.state.b);
  }
}

module.exports = WebThingAdd;
