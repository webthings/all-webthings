# WebThing: Day of the Week
This is a read-only WebThing to determine the day of the week and whether it is a weekday. Use it as an input for rules in your Web of Things gateway.

![A preview image of the WebThing Day of the Week in the Mozilla Things Gateway](preview.png)

## Explanation
The property `days since Sunday` counts the days since Sunday. Its values range from 0 on Sundays to 6 on Saturdays. This can be used to test the day of the week.

For simpler checking of the day of the week there is the property `is weekday`. It’s a true/false value and indicates whether the current day is a weekday (Monday until Friday, inclusive).
