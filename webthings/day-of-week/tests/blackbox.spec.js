const {
  SingleThing,
  WebThingServer,
} = require('webthing');
const WebThing = require('../src/webthing.js');
const WebThingHTTPTester = require('webthing-http-tester');

const PORT = 8000;

describe('test', () => {
  const thing = new WebThing();
  let server = new WebThingServer(new SingleThing(thing), PORT);
  let tester = new WebThingHTTPTester({port: PORT});

  beforeAll(async () => {
    await server.start();
  });
  afterAll(async () => {
    await server.stop();
  });
  test('matches snapshot', async () => {
    const data = await tester.getResponse('/');
    data.id = '##redacted##';
    data.links = '##redacted##';
    expect(data).toMatchSnapshot();
  });
  test('actions matches snapshot', async () => {
    const data = await tester.getActions();
    expect(data).toMatchSnapshot();
  });
  test('events matches snapshot', async () => {
    const data = await tester.getEvents();
    expect(data).toMatchSnapshot();
  });
});
