const os = require('os');
const {
  Property,
  Thing,
  Value,
} = require('webthing');

function getProperties() {
  const loadavg = os.loadavg();
  const freemem = os.freemem();
  const totalmem = os.totalmem();
  return {
    uptime: os.uptime(),
    load1min: loadavg[0],
    load5min: loadavg[1],
    load15min: loadavg[2],
    freemem: freemem,
    totalmem: totalmem,
    freememPercent: (freemem / totalmem) * 100,
  }
}

class WebThingSystemResources extends Thing {
  constructor(options = {}) {
    super(
      options.id || Math.random().toString(),
      `${os.hostname()}: System Resources`,
      ['Thing'],
      'WebThing to see the uptime information of the operating system'
    );

    const properties = getProperties();
    this.load1min = new Value(0);
    this.load5min = new Value(0);
    this.load15min = new Value(0);
    this.freemem = new Value(0);
    this.totalmem = new Value(0);
    this.freememPercent = new Value(0);
    this.uptime = new Value(0);

    this.addProperty(
      new Property(this,
        'uptime',
        this.uptime,
        {
          title: 'Uptime',
          type: 'integer',
          description: '',
          unit: 'seconds',
          minimum: 0,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'load-1min',
        this.load1min,
        {
          title: 'Avg. load (1 min)',
          type: 'number',
          description: 'Average load of the system over the last minute',
          minimum: 0,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'load-5min',
        this.load5min,
        {
          title: 'Avg. load (5 min)',
          type: 'number',
          description: 'Average load of the system over the last five minutes',
          minimum: 0,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'load-15min',
        this.load15min,
        {
          title: 'Avg. load (15 min)',
          type: 'number',
          description: 'Average load of the system over the last 15 minutes',
          minimum: 0,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'freemem',
        this.freemem,
        {
          title: 'Free memory',
          type: 'integer',
          description: 'Free memory on the system in bytes',
          // units: 'bytes',
          minimum: 0,
          maximum: os.totalmem(),
        }
      )
    );

    this.addProperty(
      new Property(this,
        'totalmem',
        this.totalmem,
        {
          title: 'Total Memory',
          type: 'integer',
          description: 'Total memory on the system in bytes',
          // units: 'bytes',
          minimum: 0,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'freemem-percent',
        this.freememPercent,
        {
          title: 'Free memory (percent)',
          type: 'number',
          description: 'Free memory divided by the total memory on the system',
          minimum: 0,
          maximum: 100,
        }
      )
    );

    this.updateProperties();
    setInterval(() => {
      this.updateProperties();
    }, 5000);
  }

  updateProperties() {
    const properties = getProperties();
    for(const key in properties) {
      this[key].notifyOfExternalUpdate(properties[key]);
    }
  }
}

module.exports = WebThingSystemResources;
