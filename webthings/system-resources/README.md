# WebThing: System Resources
This is a read-only WebThing to monitor the device it is running on. It provides the uptime, system load and amount of free memory.

![A preview image of the WebThing Add in the Mozilla Things Gateway](preview.png)

## Explanation
* `Uptime` – Counts the seconds since the last system start (boot).
* `Avg. Load` – (Unix-like only) Indicates the average load of the last 1, 5, and 15 minutes. On a quad-core a 4 means that the system has not been idle. A value of 2 would roughly translate to the system being 50% busy.
* `Free Memory` – Indicates free memory in bytes. This goes down as you open more applications.
* `Total Memory` – Indicates total memory in bytes.
* `Free Memory (percent)` – Indicates how much memory is free in relation to the total memory. This is a floating point number from 0 to 100.

The average load only works on Unix systems. On a Windows system these values remain at `0`. To get a better understanding of the values, read the man page of `uptime`. You can read this offline on your UNIX system by running `man uptime`.
