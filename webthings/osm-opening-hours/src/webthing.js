const {
  Property,
  Thing,
  Value,
} = require('webthing');
const opening_hours = require('opening_hours');

function getProperties() {
  const openingHours = new opening_hours('Mo-Th 10:00-21:00, Fr,Sa 10:00-18:00, Su 13:00-17:00');
  const date = new Date();
  return {
    open: openingHours.getState(date),
    unknown: openingHours.getUnknown(date),
    comment: openingHours.getComment(date) || '',
    nextChange: openingHours.getNextChange(date) || '',
  }
}

class WebThingOsmOpeningHours extends Thing {
  constructor() {
    super('Opening hours', ['Thing'], 'Determines whether a facility or shop is open');

    const properties = getProperties();
    this.open = new Value(properties.open);
    this.unknown = new Value(properties.unknown);
    this.comment = new Value(properties.comment);
    this.nextChange = new Value(properties.nextChange);

    this.addProperty(
      new Property(this,
        'open',
        this.open,
        {
          label: 'open',
          type: 'boolean',
          description: 'Whether it is a weekday (Monday until Friday)',
          readOnly: true,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'unknown',
        this.unknown,
        {
          label: 'unknown',
          type: 'boolean',
          description: 'Whether it is a weekday (Monday until Friday)',
          readOnly: true,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'comment',
        this.comment,
        {
          label: 'comment',
          type: 'string',
          description: 'Whether it is a weekday (Monday until Friday)',
          readOnly: true,
        }
      )
    );

    this.addProperty(
      new Property(this,
        'next-change',
        this.nextChange,
        {
          label: 'next-change',
          type: 'string',
          description: 'Whether it is a weekday (Monday until Friday)',
          readOnly: true,
        }
      )
    );

    setInterval(() => {
      this.updateProperties();
    }, 1000);
  }

  updateProperties() {
    const properties = getProperties();
    this.open.notifyOfExternalUpdate(properties.open);
    this.unknown.notifyOfExternalUpdate(properties.unknown);
    this.comment.notifyOfExternalUpdate(properties.comment);
    this.nextChange.notifyOfExternalUpdate(properties.nextChange);
  }
}

module.exports = WebThingOsmOpeningHours;
