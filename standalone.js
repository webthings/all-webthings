const fs = require('fs').promises;
const {
  SingleThing,
  WebThingServer,
} = require('webthing');
const WebThing = require('./webthing.js');

const config = {
  port: process.env.PORT || 8000,
  stateFile: process.env.STATE_FILE || 'state.json',
};

function runServer(state) {
  const thing = new WebThing({}, state);
  const server = new WebThingServer(new SingleThing(thing), config.port);

  process.on('SIGINT', () => {
    server.stop().catch(console.warn);
    const stateJson = JSON.stringify(thing.saveState());
    fs.writeFile(config.stateFile, stateJson).then(() => {
      console.log('Stored the state');
    }).catch((err) => {
      console.error(err);
      console.error('Failed to store state');
    }).finally(() => {
      process.exit();
    });
  });

  server.start().catch(console.error);
}

let state;
fs.readFile(config.stateFile).then((data) => {
  state = JSON.parse(data);
}).catch((err) => {
  console.warn(err);
  console.warn('Was not able to load a saved state.');
}).finally(() => {
  runServer(state);
});
